# Databricks notebook source
# MAGIC %md
# MAGIC
# MAGIC ## Introduction
# MAGIC
# MAGIC This notebook implements a utility class to ease logging to Azure Monitor from your notebooks. This repository is typically available in your workspace using the path `/Repos/utils/dbx-utils`
# MAGIC
# MAGIC This particular notebook can be included in a notebook as follows:
# MAGIC
# MAGIC ```
# MAGIC %run /Repos/utils/dbx-utils/monitoring
# MAGIC ```
# MAGIC
# MAGIC After which the class can be instantiated and used for metrics logging:
# MAGIC
# MAGIC ```python
# MAGIC log_connection_string = dbutils.secrets.get(
# MAGIC     dbutils.widgets.get("base_secret_scope_name"),
# MAGIC     'base-ai-connection-string'
# MAGIC )
# MAGIC
# MAGIC metric_exporter = AzureMonitorPublisher(connection_string=log_connection_string)
# MAGIC ```
# MAGIC
# MAGIC When the publisher is initialised it can be used as follows:
# MAGIC
# MAGIC ```python
# MAGIC # define dimensions for metrics
# MAGIC dimensions = {
# MAGIC     "table": table_name,
# MAGIC     "stage": stage,
# MAGIC     "step": "clean"
# MAGIC }
# MAGIC # obtain the metrics from table
# MAGIC metrics = get_table_metrics(uc_catalog, table_name)
# MAGIC
# MAGIC # log metrics in AppInsight
# MAGIC metric_exporter.track_metric(metric_name='row_count',
# MAGIC                              metric_value=metrics["rowcount"],
# MAGIC                              namespace= table_name,
# MAGIC                              dimensions=dimensions)
# MAGIC metric_exporter.track_metric(metric_name='size_in_bytes',
# MAGIC                              metric_value=metrics["sizeInBytes"],
# MAGIC                              namespace=table_name,
# MAGIC                              dimensions=dimensions)
# MAGIC metric_exporter.track_metric(metric_name='nr_of_files',
# MAGIC                              metric_value=metrics["numFiles"],
# MAGIC                              namespace=table_name,
# MAGIC                              dimensions=dimensions)# define dimensions for metrics
# MAGIC ```

# COMMAND ----------

# DBTITLE 1,Install dependencies
# MAGIC %pip install opencensus-ext-azure

# COMMAND ----------

# DBTITLE 1,MetricsPublisher class definition
import logging

from opencensus.ext.azure.common import utils
from opencensus.ext.azure.common.protocol import Data, Envelope, MetricData
from opencensus.ext.azure.log_exporter import AzureLogHandler

class AzureMonitorPublisher(AzureLogHandler):

    def __init__(self, *args, **kwargs):
        # Call the parent class's __init__ method with all arguments
        super().__init__(*args, **kwargs)

        # keep the inputs
        self.args = args
        self.kwargs = kwargs

        # Now you can initialize the child class's attributes
        self._logger = None

    @property
    def logger(self):
        if self._logger:
            return self._logger
        else:
            # Configure the logger
            logging.basicConfig(
                level=self.kwargs.get('logging_level', logging.WARNING),
                handlers=[],
            )  # Set the desired logging level
            self._logger = logging.getLogger(f'dbx-utils-{__name__}')

            # Add logging to Azure
            # self._logger.addHandler(AzureLogHandler(connection_string=self.kwargs['connection_string']))
            self._logger.addHandler(self)

            # But also to stdout
            console_handler = logging.StreamHandler()
            console_handler.setLevel(logging.INFO)  # Set the desired logging level for the console handler

            # Add the console handler to the root logger
            self._logger.addHandler(console_handler)

            print(f'Logger initialised')
            return self._logger

    def track_metric(self, metric_name, metric_value, namespace=None, dimensions=None):
        if namespace is None:
            namespace = metric_name
        if dimensions is None:
            dimensions = {}
        self._queue.put((metric_name, metric_value, namespace, dimensions), block=False)

    def log_record_to_envelope(self, record):
        if isinstance(record, tuple):
            envelope = Envelope(
                iKey=self.options.instrumentation_key,
                tags=dict(utils.azure_monitor_context),
                time=utils.to_iso_str(),
            )
            envelope.name = 'Microsoft.ApplicationInsights.Metric'
            name = record[0]
            value = record[1]
            ns = record[2]
            dimensions = record[3]
            data = MetricData(
                metrics=[
                    {
                        'kind': None,
                        'name': name,
                        'ns': ns,
                        'max': value,
                        'count': 1,
                        'value': value,
                        'min': value,
                        'stdDev': 0
                    },
                ],
                properties=dimensions
            )
            self.logger.debug("Data to log: ", data)
            self.logger.debug("- with properties :" + str(data.properties))
            envelope.data = Data(baseData=data, baseType='MetricData')
            return envelope

        return super().log_record_to_envelope(record)

# COMMAND ----------

# DBTITLE 1,Function to collect metrics from delta table

def get_table_metrics(catalog_name, table_name):

    spark.sql(f"USE CATALOG {catalog_name}")
    # get number of rows
    count = spark.sql(f"select count(*) as number_of_rows from {table_name}").collect()
    nr_of_rows = count[0].number_of_rows
    # get sizing info
    table_size = spark.sql(f"describe detail {table_name}").select("*").collect()
    size = table_size[0]["sizeInBytes"]
    files = table_size[0]["numFiles"]

    return {
        "rowcount": nr_of_rows,
        "sizeInBytes": size,
        "numFiles": files
    }

